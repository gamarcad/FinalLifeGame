package JUnit;

import liste.ListeChainee;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.TreeSet;


/**
 * La classe TestOderedLinkedList utilise le framework de test unitaire JUnit pour verifier si chacune des methodes
 * des listes chainees sont corrects.
 * 
 * @author Groupe 3
 */
public class  TestListeChainee {

     /**
     * Verifie si l'ensemble ne contient aucun element.
     */
    @Test
    public void testEstVide(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        Assert.assertEquals(true, lesEntiers.estVide());
        lesEntiers.ajouter(1);
        Assert.assertEquals(false, lesEntiers.estVide());
    }
    
    /**
     * Verifie le nombre d'éléments dans une liste.
     */
    @Test
    public void testTaille(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        Assert.assertEquals(0, lesEntiers.taille());
        lesEntiers.ajouter(1);
        Assert.assertEquals(1, lesEntiers.taille());
    }
    
     /**
     * Verifie si une liste est bien creee.
     */
    @Test
    public void testCreationListe(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        Assert.assertEquals(0, lesEntiers.taille());
    }

    /**
     * Verifie l'ajout d'un element de type entier dans une liste chainee en comparaison avec une liste de l'api.
     */
    @Test
    public void testAjoutEntier(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        TreeSet<Integer> liste_api = new TreeSet<>();

        // Ajout des entiers
        //Ajout en tête
        for(int i = 10; i >= 1; i--){
            lesEntiers.ajouter(i);
            liste_api.add(i);
        }
        //Ajout en fin
        for(int i = 12; i <= 20; i++){
            lesEntiers.ajouter(i);
            liste_api.add(i);
        }
        // Ajout au milieu
        lesEntiers.ajouter(11);
        liste_api.add(11);


        // Vérification entiers
        Iterator<Integer> it_lc = lesEntiers.iterator();
        Iterator<Integer> it_api = liste_api.iterator();

        for(Integer i =1; i <= 20; i++){
            Assert.assertEquals(true, it_api.hasNext());
            Assert.assertEquals(true, it_lc.hasNext());
            Assert.assertEquals(i, it_api.next());
            Assert.assertEquals(i, it_lc.next());
        }
    }


    /**
     * Verifie si une liste contient bien un element ajoute.
     */
    @Test
    public void testContient(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        Assert.assertEquals(false, lesEntiers.contient(1));
        lesEntiers.ajouter(12);
        Assert.assertEquals(true, lesEntiers.contient(12));
    }
    
    /**
     * Verifie si la suppression d'un element d'une liste s'effectue correctement.
     */
    @Test
    public void testSuppresionEntier(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        TreeSet<Integer> liste_api = new TreeSet<>();

        // Ajout des entiers de 1 à 20
        for(int i = 1; i <=  20; i++){
            lesEntiers.ajouter(i);
            liste_api.add(i);
        }

        // Suppression des entiers de -10 à 10
        for(int i = -10; i <= 10; i++){
            liste_api.remove(i);
            lesEntiers.supprimer(i);
        }

        // Vérification entiers
        for(Integer i =1; i <= 20; i++){
            if(i <= 10) {
                Assert.assertEquals(false, liste_api.contains(i));
                Assert.assertEquals(false, lesEntiers.contient(i));
            } else {
                Assert.assertEquals(true, liste_api.contains(i));
                Assert.assertEquals(true, lesEntiers.contient(i));
            }
        }
    }
    
     /**
     * Verifie l'ajout d'un element en tete s'effectue correctement.
     */
    @Test
    public void testAjoutTete(){
        ListeChainee<Integer> lesEntiers = new ListeChainee<>();
        for(int i = 1; i <= 20; i++)
            lesEntiers.ajouter(20);
        lesEntiers.ajouterPremier(-1);
        Assert.assertEquals(-1, lesEntiers.getTete().valeur);
    }

}
