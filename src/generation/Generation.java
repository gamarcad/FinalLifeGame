package generation;

import gestionJeu.JeuGeneration;
import liste.LC_Iterator;
import java.util.Iterator;
import liste.ListeChainee;

/**
 *  La classe Generation représente une génération du
 *  <a href="https://fr.wikipedia.org/wiki/Jeu_de_la_vie">Jeu de la vie</a>
 *  crée par <a href="https://fr.wikipedia.org/wiki/John_Horton_Conway">John Horton Conway</a>.
 *
 *  <br>Avec cette classe, il est possible de construire une succession de génération afin d'en voir le résultat.
 *  Par ailleurs il est possible de déterminer le pattern évolutif d'une génération.
 *
 *  Il existe 3 types de générations:
 *  <table>
 *      <caption>Types de générations</caption>
 *      <tr>
 *          <th>Type</th>
 *          <th>Définiton</th>
 *      </tr>
 *      <tr>
 *          <td>
 *              Génération ouverte
 *          </td>
 *          <td>
 *              Aucune limite de taille.
 *          </td>
 *      </tr>
 *      <tr>
 *          <td>
 *              Génération bouclée
 *          </td>
 *          <td>
 *              La taille d'une génération est fixée par rapport à la dimension de la première génération.
 *              De plus, une cellule au bord aura pour voisine les cellules à proximité d'elle
 *              mais aussi de l'autre côté du tableau.
 *          </td>
 *      </tr>
 *      <tr>
 *          <td>
 *              Génération fermée
 *          </td>
 *          <td>
 *              Ce type se comporte comme une génération ouverte mais la taille est fixée par rapport aux
 *              dimensions de la première génération.
 *          </td>
 *      </tr>
 *  </table>
 *
 *  @see GenerationOuverte
 *  @see GenerationBouclee
 *  @see GenerationFermee
 * 
 * @author Groupe 3
 *
 */
public abstract class Generation implements JeuGeneration, Comparable<Generation> {

    /**
     * @param lesCellules est un ensemble de cellules de la classe Cellule
     */
    protected ListeChainee<Cellule> lesCellules;
    /**
     * @param generationID est un entier permettant d'immatriculer une generation
     */
    private int generationID;
    /**
     * @param min_x
     * @param min_y
     * @param max_x
     * @param max_y
     */

    protected int min_x, max_x = 10, min_y, max_y = 10;

    /**
     * Cette methode abstrainte sera utile aux différents types de generations, ou differents mondes. Elle renvoie une instance de la classe implementant getInstance()
     * @return une instance de la classe implementante
     */
    abstract Generation getInstance();

    /**
     * Cette methode abstrainte sera utile aux différents types de generations, ou differents mondes. Elle verifie si une cellule est valide dans cette generation.
     * Par exemple, dans une GenerationFermee, il faut s'assurer que la cellule est présente dans les limites de dimensions.
     * @param c 
     *        est la cellule dont on veut obtenir la version correcte pour cette generation
     * @return true si la position de la Cellule c existe dans cette generation.
     */
    abstract boolean estValide(Cellule c);

    /**
     * Cette methode abstrainte sera utile aux différents types de generations, ou differents mondes. Elle retourne la cellule passee en parametre.
     * Par exemple, dans une GenerationBouclee, il faut "replacer" certaine cellules : si elles sortent du bas des limites de dimensions, elles sont replacees en haut.
     * @param c
     *        est la Cellule dont on veur obtenir la version correcte pour cette generation.
     * @return la Cellule aux bonnes coordonnees pour cette generation.
     * 
     */
    abstract Cellule getCellule(Cellule c);


    /**
     * Constructeur de la classe génération.
     */
    public Generation() {
        lesCellules = new ListeChainee<>();
        generationID = 1;
    }

    /**
     * Cette methode calcule le nombre de cellules voisines aux coordonnees passees en parametre.
     * Une cellule a au maximum 8 cellule =s voisines, correspondant aux 8 cellules adjacentes directement a celle de repere.
     * @param i 
     *        l'abcisse des coordonnees dont on calcule le nombre de cellules voisine
     * @param j 
     *        l'ordonnee des coordonnes dont on calcule le nombre de cellules voisine
     * @return Une liste chainee des Cellules voisines aux coordonnes passees en parametre.
     */
    public ListeChainee<Cellule> getVoisines(int i, int j) {
        ListeChainee<Cellule> res = new ListeChainee<>();
        if (i == 0 && j == 0) {
            for (Cellule cell : lesCellules) {
                Cellule c = new Cellule(cell.getX() + i, cell.getY() + j, 10);
                if (estValide(c))
                    res.ajouterPremier(getCellule(c));
            }
        } else {
            for (Cellule cell : lesCellules) {
                Cellule c = new Cellule(cell.getX() + i, cell.getY() + j, 1);
                if (estValide(c))
                    res.ajouterPremier(getCellule(c));
            }
        }
        return res;
    }


    /**
     * Calcule et retourne la génération suivante de la génération appelante.
     *
     * @return Generation La génération suivante par rapport à this.
     */
    public Generation nextGeneration() {
        Generation g = getInstance();
        g.generationID = generationID + 1;
        if (lesCellules.estVide())
            return g;

        ListeChainee<Cellule> res = getVoisines(1, 1);
        ListeChainee<Cellule> voisines;

        for (int y = 1; y >= -1; y--) {
            for (int i = 1; i >= -1; i--) {
                if (i != 1 || y != 1) {
                    LC_Iterator<Cellule> it = (LC_Iterator) res.iterator();
                    voisines = getVoisines(i, y);

                    for (Cellule voisine : voisines) {
                        while (it.hasNext()) {
                            Cellule aux = (Cellule) it.getValeur();
                            int n = voisine.compareTo(aux);
                            if (n == -1) {
                                if (it.suivantNull()) {
                                    it.ajouterApres(voisine);
                                    break;
                                }
                                it.suivant();
                            } else {
                                if (n == 1)
                                    it.ajouterAvant(voisine);
                                else
                                    aux.addNbVoisins(voisine);
                                break;
                            }
                        }
                    }
                    voisines.nettoyer();
                }
            }
        }
        return traitementAffichage(g, res);
    }

    protected Generation traitementAffichage(Generation g, ListeChainee<Cellule> cellules) {
        ListeChainee<Cellule> pointeurListe = g.lesCellules;
        for (Cellule c : cellules) {
            int nb_voisins = c.getVoisins();
            if (nb_voisins >= 10) {
                nb_voisins = nb_voisins % 10;
                if (nb_voisins == 2 || nb_voisins == 3) {
                    pointeurListe.ajouterPremier(c);
                }
            } else if (nb_voisins == 3) {
                pointeurListe.ajouterPremier(c);
            }

        }
        g.min_x = min_x;
        g.max_x = max_x;
        g.min_y = min_y;
        g.max_y = max_y;
        return g;
    }

    /**
     * Retourne true si la Generration est vide
     * @return true si la Generration est vide
     */
    @Override
    public boolean estVide() {
        return lesCellules.estVide();
    }

    /**
     * Ajoute une cellule dans la génération.
     * Cette fonction ne prend en paramètres que les coordonnées de la cellule à ajouter.
     *
     * @param x Abscisse de la cellule.
     * @param y Ordonnée de la cellule.
     */
    public void ajouterCellule(int x, int y) {

        ajouterCellule(new Cellule(x, y, 10));
    }


    /**
     * Ajoute une cellule dans la génération.
     *
     * @param c La cellule à ajouter.
     */
    public void ajouterCellule(Cellule c) {
        int x = c.getX(), y = c.getY();
        if (x < min_x) min_x = x;
        if (x > max_x) max_x = x;
        if (y < min_y) min_y = y;
        if (y > max_y) max_y = y;
        lesCellules.ajouter(c);
    }

    /**
     * Retourne la chaîne de caractères correspondant à la génération.
     *
     * @return String La génération sous forme de caractères.
     */
    public String toString() {
        StringBuilder res = new StringBuilder("GID: ").append(generationID).append("\n");
        if (lesCellules.estVide()) return res.append("Aucune cellule a afficher dans cette generation\n").toString();
        res.append("Nombre de cellules: ").append(lesCellules.taille()).append("\n");
        Iterator<Cellule> it = lesCellules.iterator();
        Cellule aux = it.next();
        boolean vide = false;
        for (int y = min_y; y <= max_y; y++) {
            for (int x = min_x; x <= max_x; x++) {
                if (!vide && aux.getX() == x && aux.getY() == y) {
                    res.append(" \u25cf ");
                    if (it.hasNext())
                        aux = it.next();
                    else
                        vide = true;
                } else
                    res.append(" \u25e6 ");

            }
            res.append("\n");
        }
        return res.toString();
    }

    /**
     * Retourne un booléen {@code true} si {@code o} est équivalent à {@code this}.
     *
     * @param o L'objet comparé.
     * @return bool {@code true} si o est équivalent à {@code this}, {@code false} sinon.
     */
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Generation))
            return false;
        if (this == o)
            return true;
        Generation g = (Generation) o;
        return lesCellules.equals(g.lesCellules);
    }

    /**
     * Cette methode permet de comparer 2 Generations entre elles. Elle retourne un entier soit negatif, nul ou positif suivant si les deux generations sont totalement différentes, egales ou decalees.
     * En clair, si la generation passee en parametre possede des cellules aux memes coordonnes que l'objet appelant, aors les generations sont egales. Si on retrouve les cellules mais toutes decalees avec le meme coefficient, alors les gnerations sont decalees. Si les generations ne possedent aucun lien entre elles, elles sont differentes.
     * @param o 
     *        la Generation que l'on souhaite comparer avec la Generation appelante 
     * @return un entier soit negatif, nul ou positif suivant si les deux generations sont totalement différentes, egales ou decalees.
     * 
     */
    @Override
    public int compareTo(Generation o) {
        ListeChainee<Cellule> lc1 = lesCellules;
        ListeChainee<Cellule> lc2 = o.lesCellules;

        if (lc1 == null || lc2 == null)
            return -1;

        if (lc1.taille() != lc2.taille())
            return -1;

        Iterator<Cellule> it1 = lc1.iterator();
        Iterator<Cellule> it2 = lc2.iterator();

        int decalageX = ((Cellule) lc2.getTete().valeur).getX() - ((Cellule) lc1.getTete().valeur).getX();
        int decalageY = ((Cellule) lc2.getTete().valeur).getY() - ((Cellule) lc1.getTete().valeur).getY();

        int tmpX = 0;
        int tmpY = 0;

        Cellule c1;
        Cellule c2;

        while (it1.hasNext()) {
            c1 = it1.next();
            c2 = it2.next();
            tmpX = c2.getX() - c1.getX();
            tmpY = c2.getY() - c1.getY();
            if (tmpX != decalageX || tmpY != decalageY)
                return -1;
        }

        if (decalageX == 0 && decalageY == 0)
            return 0;
        return 1;
    }

    /**
     * Cette méthode doit définir le comportement des générations succedant la generation initiale, avec une limite de generation maximale, passee en parametre.
     * On retourne un tableau à 3 cases avec  :
     * Dans la premiere case =
     * Un entier entre 0 et 4 (inclus) suivant si le comportement est inconnu, mort, stable, oscillateur ou vaisseau
     * Dans la seconde case =
     * Un entier correspondant a taille de la queue
     * Dans la derniere case
     * Un entier correspondant a taille de la boucle
     * 
     * @param t
     *        limite de generation maximale pour etudier le comportement des Generations
     * @return un tableau d'entier remplit suivantt la description ci-dessus
     */
    public int[] typeEvolution(int t) {

        Generation g1 = this;
        Generation g2 = this.nextGeneration();

        int majQueue = 0;
        int[] tab = new int[3];
        int res;

        while (!(g1.generationID == t)) {

            if (g1.lesCellules.estVide()) {
                tab[0] = 1;
                tab[1] = majQueue;
                tab[2] = 0;
                return tab;
            }

            if (g1.equals(g1.nextGeneration())) {
                tab[0] = 2;
                tab[1] = majQueue;
                tab[2] = 1;
                return tab;
            }

            int comp = g1.compareTo(g2);
            if (comp != -1) {
                if (comp == 0)
                    tab[0] = 3;
                else
                    tab[0] = 4;

                res = 1;
                g2 = g2.nextGeneration();
                while (g1.compareTo(g2) == -1) {
                    res++;
                    g2 = g2.nextGeneration();
                }
                tab[2] = res;

                res = 0;
                g1 = this;
                while (res <= majQueue) {
                    for (int i = 0; i < tab[2]; i++) {
                        if (g1.compareTo(g2) != -1) {
                            tab[1] = res;
                            return tab;
                        }
                        g2 = g2.nextGeneration();
                    }
                    res++;
                    g1 = g1.nextGeneration();
                }
            }

            g1 = g1.nextGeneration();
            g2 = g2.nextGeneration().nextGeneration();
            majQueue++;
        }

        tab[0] = 0;
        tab[1] = majQueue + 1;
        tab[2] = 0;
        return tab;
    }
}